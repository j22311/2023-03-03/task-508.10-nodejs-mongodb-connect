//const { default: mongoose } = require("mongoose");

//b1 khai báo thưu viện mongodb
const mongoose = require('mongoose');

//b2 khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//b3: tạo đối tượng Schema gồm các thuộc tính của Collection
const reviewSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        require: false
    },
    create_At: {
        type: Date,
        default: Date.now()
    },
    update_At: {
        type: Date,
        default: Date.now()
    }
});

//b4: export Schema ra model
module.exports = mongoose.model("review",reviewSchema)
//review là tên sẽ tự sinh ra trong CRUD_Course 