//const { default: mongoose } = require("mongoose");

//b1:khai báo thưu viện MOnggodb
const mongoose = require("mongoose");

//b2: khai báo thư viện Schema(để tạo một database)
const Schema = mongoose.Schema;

//b3: tạo đối tượng Schema bao gồm các thuộc tính collelction trng monggbd
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        require: true,
        unique: true
    },
    description :{
        type: String,
        require: false
    },
    noStudent : {
        type: Number,
        default: 0
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "review"
        }
    ]
})

//b4: export SChema ra model
module.exports = mongoose.model("course",courseSchema);
//course là tên sẽ tự sinh ra trong CRUD_Course 
