// khai báo thư viện 
const express = require("express");

// khai báo middleware 
const {
   
    putAcourseMIddleware,
    postAcourseMIddleware,
    getAcourseMIddleware,
    getAllcourseMIddleware,
    deleteAcourseMIddleware
} = require('../middlewares/courseMIddleware');

//tạo Router
const courseRouter = express.Router();
//gọi ROuter cho all course
courseRouter.get("/courses",getAllcourseMIddleware, (req,res) => {
    res.json({
        message: "Get all course"
    });
})

//gọi ROuter cho 1 course
courseRouter.get("/courses/:idCourse",getAcourseMIddleware, (req,res) => {
    let idCourse = req.params.idCourse
    res.json({
        message: `Get a course idcoure = ${idCourse}` 
    });
})

// gọi Router to create a course 
courseRouter.post("/courses",postAcourseMIddleware, (req,res) => {
    res.json({
        message: "create a course"
    });
})
// gọi Router update cho 1 course 
courseRouter.put("/courses/:courseId",putAcourseMIddleware, (req,res) => {
    let courseId = req.params.courseId
    res.json({
        message: `update a course ID = ${courseId}`
    });
})

// gọi Router delete cho 1 course 
courseRouter.delete("/courses/:courseId",deleteAcourseMIddleware, (req,res) => {
    let courseId = req.params.courseId
    res.json({
        message: `delete a course ID = ${courseId}`
    });
})
module.exports = {
    courseRouter
}